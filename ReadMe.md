# Secret Message

### In the Secret Message mini project, we'll write a secret message for a friend, and then write a program to decode the message! 

# Input secret message given
![Alt text](https://bitbucket.org/somashekarm/secret-message/raw/635d3f6fabc6722d6df523df38bf1cb4efd05a11/Input.PNG)

# Output decoded message
![Alt text](https://bitbucket.org/somashekarm/secret-message/raw/2ab2bfa10818cbbfda3edee74f9812668fc42a71/Output.PNG)

