import os
def rename_files():
    
    # gets the file names from a folder and store it in a variable file_list
    # r take the rawpath don't interpret otherway
    file_list = os.listdir(r"C:\Users\james\Desktop\intro to python programming\use functions\Secret Message\prank")
    
    # print the filenames from the list 
    print (file_list)

    # get the current working directory and save it in saved_path
    saved_path = os.getcwd()

    #print current working directory 
    print"current working directory is"+saved_path

    # look in the folder where the files actually resides
    os.chdir(r"C:\Users\james\Desktop\intro to python programming\use functions\Secret Message\prank")

    # for each file, rename filename
    for file_name in file_list:
        # Old file name
         print file_list
        # New file name
         print file_name.translate(None,"0123456789")
        #rename the file name by removing 0123456789 
         os.rename(file_name,file_name.translate(None,"0123456789"))
        
    # change the path back to original   
    os.chdir(saved_path)     
rename_files()    
    
